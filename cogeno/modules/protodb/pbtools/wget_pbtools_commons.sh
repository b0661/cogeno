#!/usr/bin/env sh

wget -N -c https://raw.githubusercontent.com/eerimoq/pbtools/master/lib/src/pbtools.c
wget -N -c https://raw.githubusercontent.com/eerimoq/pbtools/master/lib/include/pbtools.h
wget -N -c https://raw.githubusercontent.com/eerimoq/pbtools/master/LICENSE