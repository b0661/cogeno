#!/usr/bin/env sh

wget -N -c https://raw.githubusercontent.com/nanopb/nanopb/master/pb.h
wget -N -c https://raw.githubusercontent.com/nanopb/nanopb/master/pb_common.c
wget -N -c https://raw.githubusercontent.com/nanopb/nanopb/master/pb_common.h
wget -N -c https://raw.githubusercontent.com/nanopb/nanopb/master/pb_decode.c
wget -N -c https://raw.githubusercontent.com/nanopb/nanopb/master/pb_decode.h
wget -N -c https://raw.githubusercontent.com/nanopb/nanopb/master/pb_encode.c
wget -N -c https://raw.githubusercontent.com/nanopb/nanopb/master/pb_encode.h
wget -N -c https://raw.githubusercontent.com/nanopb/nanopb/master/generator/proto/nanopb.proto
wget -N -c https://raw.githubusercontent.com/nanopb/nanopb/master/LICENSE.txt