#! /usr/bin/env sh
# Install protobluff to user local

# Ubuntu
sudo apt install git gcc automake libtool protobuf-compiler libprotoc-dev libprotobuf-dev check

# get the sources
git clone https://github.com/squidfunk/protobluff protobluff.git
cd protobluff.git

# configure
./autogen.sh
./configure --prefix ${HOME}/.local

# build & test & install to user dir
make
make test
make install

# remove the git repository
cd ..
rm -rf protobluff.git
