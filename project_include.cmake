# Copyright (c) 2020 Bobby Noelte.
# SPDX-License-Identifier: Apache-2.0

# Cogeno as a component for ESP IDF/MDF
if(DEFINED ESP_PLATFORM)
    # Use default cogeno cmake script
    include(${COMPONENT_DIR}/examples/cmake/cogeno.cmake)
endif()
