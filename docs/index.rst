..
    Copyright (c) 2018..2020 Bobby Noelte
    SPDX-License-Identifier: Apache-2.0

.. _cogeno:

Cogeno's documentation!
#######################

.. toctree::
    :maxdepth: 1

    welcome
    cogeno/about
    cogeno/getting_started
    cogeno/invoke_cogeno
    cogeno/scripting
    cogeno/extensions
    cogeno/modules
    cogeno/templates
    cogeno/build
    cogeno/development
    cogeno/faq
