..
    Copyright (c) 2018..2020 Bobby Noelte
    SPDX-License-Identifier: Apache-2.0

@code{.cogeno.py}
from pathlib import Path
cogeno.import_module('rstcode')
generated_dir = Path(__file__).parent.parent.joinpath('generated')
@endcode{.cogeno.py}
@code{.cogeno.ins}@endcode

.. _generated_index:

Generated Content Index
#######################

.. toctree::
    :maxdepth: 1

@code{.cogeno.py}
generated_files =  generated_dir.glob(f'**/*.rst')
for generated_file in sorted(generated_files):
    if generated_file.name == 'generated_index.rst':
        continue
    cogeno.out(f'    {generated_file.stem}\n')
@endcode{.cogeno.py}
@code{.cogeno.ins}@endcode
