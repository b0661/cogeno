..
    Copyright (c) 2018..2020 Bobby Noelte
    SPDX-License-Identifier: Apache-2.0

@code{.cogeno.py}
from pathlib import Path
cogeno.import_module('rstcode')
generated_dir = Path(__file__).parent.parent.joinpath('generated')
bindings_dir = Path(__file__).parents[2].joinpath('cogeno/modules/edtsdb/dts/bindings')
@endcode{.cogeno.py}
@code{.cogeno.ins}@endcode

.. _cogeno_edts_bindings_index:

EDTS Bindings Index
###################

@code{.cogeno.py}
bindings =  bindings_dir.glob(f'**/*.yaml')
rst_binding_index = rstcode.BulletList()
for binding_file in sorted(bindings):
    with binding_file.open('r') as binding_fd:
        rst_binding_target = rstcode.sanitize_target(f'binding_{binding_file.stem}')
        rst_binding_file_name = f'{rst_binding_target.lower()}.rst'
        rst_binding_file_path = str(generated_dir.joinpath(rst_binding_file_name))
        rst_binding_header = f'{binding_file.stem}'
        # add link to index list
        rst_binding_index.add_item(rstcode.link_reference(rst_binding_target))
        # write binding documentation rst file
        rst_binding = rstcode.Document(file_path = rst_binding_file_path)
        rst_binding.add_child(rstcode.Comment(\
"""Copyright (c) 2020 Bobby Noelte
SPDX-License-Identifier: Apache-2.0"""))
        rst_binding.add_child(rstcode.LinkTarget(rst_binding_target))
        rst_binding.add_child(rstcode.Section(rst_binding_header))
        rst_binding.add_child(rstcode.CodeBlock(code = f'include: {binding_file.name}\n', code_type = 'yaml'))
        rst_binding.add_child(rstcode.CodeBlock(code = binding_fd.read(), code_type = 'yaml'))
        rst_binding.out()
rst_binding_index.out()
@endcode{.cogeno.py}
@code{.cogeno.ins}@endcode
