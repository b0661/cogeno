..
    Copyright (c) 2018..2020 Bobby Noelte
    SPDX-License-Identifier: Apache-2.0

.. _cogeno_welcome:
    
Welcome to Cogeno's documentation!
##################################

This documentation is continuously written. It is edited via text files in the
reStructuredText markup language and then compiled into a static website/
offline document using the open source Sphinx and
`Read the Docs <https://cogeno.readthedocs.io/en/latest/index.html>`_ tools.

You can contribute to cogeno's dcoumentation by opening
`GitLab issues <https://gitlab.com/b0661/cogeno/issues>`_
or sending patches via merge requests on its
`GitLab repository <https://gitlab.com/b0661/cogeno>`_.

Sections
========

* :ref:`cogeno_about`
* :ref:`cogeno_getting_started`
* :ref:`cogeno_invoke_cogeno`
* :ref:`cogeno_scripting`
* :ref:`cogeno_extensions`
* :ref:`cogeno_modules`
* :ref:`cogeno_templates`
* :ref:`cogeno_build`
* :ref:`cogeno_development`
* :ref:`cogeno_faq`

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

..
    Include generated sources here to make them available for referencing

.. toctree::
    :maxdepth: 1

    generated/generated_index


