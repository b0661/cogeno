..
    Copyright (c) 2018..2020 Bobby Noelte
    SPDX-License-Identifier: Apache-2.0

.. _cogeno_getting_started:

Getting started with Cogeno
###########################

.. contents::
   :depth: 1
   :local:
   :backlinks: top


Set up an environment for Cogeno
********************************

To start off, be sure that you have installed a fairly recent Python 3 and
the Python package manager pip3. Git is needed in case you want to get the
source.

Get Cogeno
**********

Get the source
==============

Cogeno's latest version is available from `<https://gitlab.com/b0661/cogeno>`_::

    git clone https://gitlab.com/b0661/cogeno.git

Install Cogeno
==============

Cogeno can be installed from `PyPi <https://pypi.org/project/cogeno/>`_::

    pip3 install cogeno

Cogeno's latest version can be installed by::

    git clone https://gitlab.com/b0661/cogeno.git
    pip3 install -e cogeno

Run Cogeno
**********

To run from source one time assure all dependencies are installed::

    pip3 install -r <cogeno repo clone>/requirements.txt

Then call::

    python3 <cogeno repo clone>/cogeno/cogeno.py <any option>

To run the installed variant you only have to call::

    cogeno <any option>

For all the possible options see :ref:`cogeno_invoke_cogeno`.

Integrate Cogeno into your project´s build system
*************************************************

Cogeno usually is invoked during the build process by your favourite build system.

Cogeno provides example solutions for some of them (see :ref:`cogeno_build`),
especially for the `CMake <https://cmake.org/>`_ build system generator.

Get Cogeno´s documentation
**************************

Cogeno´s documentation is available online at
`Read the Docs <https://cogeno.readthedocs.io/en/latest/index.html>`_.

The source of the documentation is in the ``docs`` folder.

To build it first assure all dependencies are fulfilled::

    pip3 install -r <cogeno repo clone>/cogeno/docs/requirements.txt

You can then create it by::

    cd <cogeno repo clone>/cogeno/docs
    make html
