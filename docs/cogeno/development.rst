..
    Copyright (c) 2018..2020 Bobby Noelte
    SPDX-License-Identifier: Apache-2.0

.. _cogeno_development:

Development
###########

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   principle
   cogeno_api
   edts_api
   cmake_api
