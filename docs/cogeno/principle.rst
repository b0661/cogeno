..
    Copyright (c) 2018 Bobby Noelte
    SPDX-License-Identifier: Apache-2.0

.. _cogeno_principle:

Code generation principle
#########################

How code generation works with cogeno.

.. contents::
   :depth: 2
   :local:
   :backlinks: top

Principle
---------

.. image:: cogeno_principle.png
   :width: 750px
   :align: center
   :alt: Principle

Inclusion of other inline code
------------------------------

.. image:: cogeno_principle_include.png
   :width: 750px
   :align: center
   :alt: Include other inline code

Access to project data
----------------------

.. image:: cogeno_principle_access.png
   :width: 750px
   :align: center
   :alt: Access project data

Import of Python modules
------------------------

.. image:: cogeno_principle_import.png
   :width: 750px
   :align: center
   :alt: Import Python modules

