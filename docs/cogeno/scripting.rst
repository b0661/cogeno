..
    Copyright (c) 2018 Bobby Noelte
    SPDX-License-Identifier: Apache-2.0

.. _cogeno_scripting:

Scripting with Cogeno
#####################

.. toctree::
   :maxdepth: 2

   functions
   modules
   templates
