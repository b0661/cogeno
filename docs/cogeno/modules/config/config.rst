..
    Copyright (c) 2020 Bobby Noelte
    SPDX-License-Identifier: Apache-2.0

.. _cogeno_modules_config:

Config/ Kconfig support (config)
################################

.. contents::
   :depth: 1
   :local:
   :backlinks: top

Description
***********

The config module provides access to config properties and the config database.

You may get access to the database by :func:`cogeno.configs()`.

Cogeno invocation options
*************************

``--config:db FILE``
    Write or read config database to/ from FILE.

``--config:file FILE``
    Read configuration variables from this FILE.

``--config:kconfig-file FILE``
    Top-level Kconfig FILE (default: Kconfig).

``--config:kconfig-srctree DIR``
    Kconfig files are looked up relative to the srctree DIR (unless absolute paths
    are used), and .config files are looked up relative to the srctree DIR if they
    are not found in the current directory.

``--config:kconfig-defines DEFINE [DEFINE ...]``
    Define variable to Kconfig. We allow multiple.

``--config:inputs FILE [FILE ...]``
    Read configuration file fragment from FILE. We allow multiple.

.. _cogeno_modules_config_functions:

Code generation functions
*************************

There are convenience functions to access the config properties:

.. doxygenfunction:: cogeno::stdmodules::StdModulesMixin::configs()
    :project: cogeno

.. doxygenfunction:: cogeno::stdmodules::StdModulesMixin::config_properties()
    :project: cogeno

.. doxygenfunction:: cogeno::stdmodules::StdModulesMixin::config_property()
    :project: cogeno

The config database itself provides a more rich set of methods for configuration access:

.. doxygenclass:: cogeno::modules::config::ConfigDB
    :project: cogeno
    :members:

Generate code with config
*************************

.. toctree::
   :maxdepth: 3

   kconfig_guide
