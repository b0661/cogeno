..
    Copyright (c) 2015-2020 Zephyr Project members and individual contributors
    Copyright (c) 2020 Bobby Noelte
    SPDX-License-Identifier: Apache-2.0

.. _kconfig_guide:

Kconfig Guide
#############

This is a high-level guide to Kconfig and how to use it with Cogeno.
See :ref:`config code generation functions <cogeno_modules_config_functions>`
for an API reference.

Introduction to Kconfig
=======================

Projects can be configured by Kconfig at build time to adapt them for specific
application and platform needs. The goal is to support configuration without
having to change any source code. Kconfig is the configuration system used by the
`Linux kernel <https://www.kernel.org/doc/html/latest/kbuild/kconfig-language.html>`_
and several other projects. 

Configuration options (often called symbols) are defined in Kconfig files, which also specify
dependencies between symbols that determine what configurations are valid. Symbols can be grouped
into menus and sub-menus to keep the interactive configuration interfaces organized.

The usual output from Kconfig is a header file ``autoconf.h`` with macros that can be tested at
build time. Kconfig itself keeps the values of the configuration symbols in a config file usually
named ``.config``. Kconfig can either read the config file and produce output or generate or change
the config file using one of several interactive configuration interfaces.

The values of the configuration symbols can also be changed by hand editing a Kconfig file and let
Kconfig update the config file and the output.

(K)config access from Cogeno
============================

Cogeno includes a complete Kconfig system (provided by `Kconfiglib <https://github.com/ulfalizer/Kconfiglib>`_) 
but no interactive configuration interfaces.

The Cogeno :ref:`config module <cogeno_modules_config>` creates it´s own database of configuration symbol
values. It may either

1) extract the symbol names and values from a config file provided to Cogeno as an option or
2) run the whole process of symbol and value generation based on Kconfig files, and config file and/ or
   config fragment files also provided to Cogeno as options.

In the first case only the name and value of the symbols are known to the :ref:`config module <cogeno_modules_config>`,
whereas in the second case additional symbol information is available from the Kconfig files.

Property access
***************

Use :func:`cogeno.config_property(<symbol>)` to get the value of a single symbol.

Use :func:`cogeno.config_properties()` to get a dictionary of symbols with their 
associated values.




