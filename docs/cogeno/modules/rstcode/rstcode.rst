..
    Copyright (c) 2020 Bobby Noelte
    SPDX-License-Identifier: Apache-2.0

.. _cogeno_modules_rstcode:

ReST/ RST code generation (rstcode)
###################################

.. contents::
   :depth: 1
   :local:
   :backlinks: top

Description
***********

The rstcode module supports code generation for the 
`reST (RST) markup language <https://docutils.sourceforge.io/docs/ref/rst/restructuredtext.html>`_.

To use the module in inline code generation import it by:

::

    cogeno.import_module('rstcode')

In case you want to use the rstcode module in another Python
project import it by:

::

    import cogeno.modules.rstcode

Cogeno invocation options
*************************

There are **NO** Cogeno invocation options.

Code generation functions
*************************

.. doxygenfunction:: cogeno::modules::rstcode::sanitize_target()
    :project: cogeno

.. doxygenfunction:: cogeno::modules::rstcode::link_reference()
    :project: cogeno

.. doxygenclass:: cogeno::modules::rstcode::Node
    :project: cogeno
    :members:

.. doxygenclass:: cogeno::modules::rstcode::Text
    :project: cogeno
    :members:

.. doxygenclass:: cogeno::modules::rstcode::Comment
    :project: cogeno
    :members:

.. doxygenclass:: cogeno::modules::rstcode::CodeBlock
    :project: cogeno
    :members:

.. doxygenclass:: cogeno::modules::rstcode::Paragraph
    :project: cogeno
    :members:

.. doxygenclass:: cogeno::modules::rstcode::Section
    :project: cogeno
    :members:

.. doxygenclass:: cogeno::modules::rstcode::BulletList
    :project: cogeno
    :members:

.. doxygenclass:: cogeno::modules::rstcode::OrderedList
    :project: cogeno
    :members:

.. doxygenclass:: cogeno::modules::rstcode::Table
    :project: cogeno
    :members:

.. doxygenclass:: cogeno::modules::rstcode::LinkTarget
    :project: cogeno
    :members:

.. doxygenclass:: cogeno::modules::rstcode::Document
    :project: cogeno
    :members:
