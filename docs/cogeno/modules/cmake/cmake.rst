..
    Copyright (c) 2018..2020 Bobby Noelte
    SPDX-License-Identifier: Apache-2.0

.. _cogeno_modules_cmake:

CMake support (cmake)
#####################

.. contents::
   :depth: 1
   :local:
   :backlinks: top

Description
***********

The CMake module provides access to CMake variables and the CMake cache.

You may get access to the database by :func:`cogeno.cmake()`.

Cogeno invocation options
*************************

``--cmake:cache FILE``
    Use CMake variables from CMake cache FILE.

``--cmake:define [defxxx=valyyy ...]``
    Define CMake variables to your generator code.

Code generation functions
*************************

There are convenience functions to access the CMake variables:

.. doxygenfunction:: cogeno::stdmodules::StdModulesMixin::cmake()
    :project: cogeno

.. doxygenfunction:: cogeno::stdmodules::StdModulesMixin::cmake_variable()
    :project: cogeno

.. doxygenfunction:: cogeno::stdmodules::StdModulesMixin::cmake_cache_variable()
    :project: cogeno

The CMake database may be used directly to get access to the CMake variables:

.. doxygenclass:: cogeno::modules::cmake::CMakeDB
    :project: cogeno
    :members:

