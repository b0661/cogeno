..
    Copyright (c) 2018..2020 Bobby Noelte
    SPDX-License-Identifier: Apache-2.0

.. _cogeno_modules_zephyr:

Zephyr support (zephyr)
#######################

.. contents::
   :depth: 1
   :local:
   :backlinks: top

Description
***********

The Zephyr module supports code generation for the Zephyr RTOS.

To use the module in inline code generation import it by:

::

    cogeno.import_module('zephyr')

In case you want to use the Zephyr module in another Python
project import it by:

::

    import cogeno.modules.zephyr

Cogeno invocation options
*************************

There are **NO** Cogeno invocation options.

Code generation functions
*************************

.. doxygenfunction:: cogeno::modules::zephyr::str2ident()
    :project: cogeno

.. doxygenfunction:: cogeno::modules::zephyr::device_name_by_id()
    :project: cogeno

.. doxygenfunction:: cogeno::modules::zephyr::device_declare_single()
    :project: cogeno

.. doxygenfunction:: cogeno::modules::zephyr::device_declare_multi()
    :project: cogeno

Generate code with zephyr
*************************

.. toctree::
   :maxdepth: 3

   zephyr_device_declare
   zephyr_api

