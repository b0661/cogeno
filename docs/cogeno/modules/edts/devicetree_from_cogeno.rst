..
    Copyright (c) 2015-2020 Zephyr Project members and individual contributors
    Copyright (c) 2020 Bobby Noelte
    SPDX-License-Identifier: Apache-2.0

.. _devicetree-from-cogeno:

Devicetree access from Cogeno
#############################

This guide describes Cogeno's :ref:`cogeno_edts_api` for reading the devicetree
information from the EDTS database. It assumes you're familiar with the concepts in
:ref:`devicetree-intro` and :ref:`devicetree-bindings`. See :ref:`cogeno_edts_api` for
API reference documentation.

.. _devicetree-device-identifiers:

Device identifiers
******************

To get information about a particular devicetree device node, you need a
*device identifier* for it. This is just a key that refers to the device node.

These are the main ways to get a device identifier:

By path
   Use the the device node's ``full path in the devicetree``, starting from the root node
   as device identifier (e.g. ``/soc/i2c@40002000``). This is mostly useful if you happen
   to know the exact node you're looking for.

By node name
   Use ``cogeno.edts().device_id_by_name(<node name>)`` to get a device identifier
   from a device `node name` (e.g. ``i2c@40002000``).

By node label
   Use ``cogeno.edts().device_id_by_name(<node label name>)`` to get a device identifier from
   the :ref:`node label <dt-node-labels>` of a device node.
   The function first searches for a matching `node name` before looking into the
   `node labels` of a device node. `node labels` are often provided by SoC
   :file:`.dtsi` to give nodes names that match the SoC datasheet, like ``i2c1``,
   ``spi2``, etc.

By label property
   Use ``cogeno.edts().device_id_by_name(<node label property name>)`` to get a
   device identifier from the `label property` of a device node.
   The function first searches for a matching `node name` before looking into the
   :ref:`node label <dt-node-labels>` and finally checks the `label` property.
   `label` properties are often provided by board :file:`.dtsi` to give device nodes
   names that match the board interface, like ``I2C_1``, ``SPI_2``, etc.

By alias
   Use ``cogeno.edts().device_id_by_alias(<alias name>)`` to get a device node identifier
   for a property of the special `/aliases` node (e.g. ``sensor-controller``).

By instance number
   Use ``cogeno.edts().device_ids_by_compatible(<compatible>)[<instance number>]``
   to get the device identifier of an individual device node based on a matching compatible,
   but be careful doing so. See below.

By chosen node
   Use ``cogeno.edts().device_id_by_chosen(<chosen property>)`` to get a device identifier for
   a `/chosen` node property (e.g. ``zephyr,console``).

By parent
   Use ``cogeno.edts().device_property(<device id>, 'parent')`` to get a device identifier
   for a parent device node, starting from a device identifier you already have.


Two device identifiers which refer to the same device node are identical and can be used
interchangeably.

.. _dt-node-main-ex:

Here's a DTS fragment for some imaginary hardware we'll return to throughout
this file for examples:

.. literalinclude:: devicetree_example.dts
   :language: DTS
   :start-after: start-after-here

Here are a few ways to get the device identifier for the ``i2c@40002000`` node:

- ``'/soc/i2c@40002000'``
- ``cogeno.edts().device_id_by_name('i2c@40002000')``
- ``cogeno.edts().device_id_by_name('i2c1')``
- ``cogeno.edts().device_id_by_name('I2C_1')``
- ``cogeno.edts().device_id_by_alias('sensor-controller')``
- ``cogeno.edts().device_ids_by_compatible('vnd,soc_i2c')[x]`` for some unknown number ``x``.

Property access
***************

The right API to use to read property values depends on the format the property values
shall be provided.

.. _dt-checking-property-exists:

Checking properties and values
==============================

You can use ``cogeno.edts().device_property(<device id>, <property path>, None)`` to check
if a device node has a property `<property path>`. ``None`` will be returned if the property
does not exist.

.. _devicetree-single-property:

Single property
===============

Use ``cogeno.edts().device_property(<device id>, <property path>, <default value>)`` to
read a single property.

.. _devicetree-multiple-properties:

Multiple properties
===================

Multiple properties at the same time can be read in several ways.

Use ``cogeno.edts().device_properties(<device_id>)`` to get a dictionary of dictionaries
representing the properties of a device node.

Use ``cogeno.edts().device_properties_flattened(<device_id>. <path prefix>)`` to
get the properties flattened to **one** key-value dictionary where the keys
represent the property path.

Use ``cogeno.edts().device_template_substitute(<device_id>, <template>, <presets>, <aliases>)``
to get the property values requested by the placeholders in the template.
