..
    Copyright (c) 2018..2020 Bobby Noelte
    SPDX-License-Identifier: Apache-2.0

.. _cogeno_edts_bindings:

EDTS bindings
#############

The EDTS database module uses :ref:`devicetree bindings <devicetree-bindings>`
(a kind of data schema) to know what data to extract and to know the kind of data.
A set of :ref:`generic devicetree bindings <cogeno_edts_bindings_index>` in conjunction
with project specific bindings control the extraction process.

The :ref:`generic bindings <cogeno_edts_bindings_index>` are part of the EDTS database module.

    * :ref:`cogeno_edts_bindings_index`




