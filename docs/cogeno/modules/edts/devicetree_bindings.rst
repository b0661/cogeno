..
    Copyright (c) 2015-2020 Zephyr Project members and individual contributors
    Copyright (c) 2020 Bobby Noelte
    SPDX-License-Identifier: Apache-2.0

.. _devicetree-bindings:

Devicetree bindings
###################

A devicetree on its own is only half the story for describing hardware. The
devicetree format itself is relatively unstructured, and doesn't tell the
Cogeno EDTS module which pieces of information in a particular devicetree
are useful to be integrated into the EDTS database.

*Devicetree bindings* provide the other half of this information. Cogeno EDTS
devicetree bindings are YAML files in a custom format (Cogeno uses the Zephyr
bindings and extends them in some rare cases. It does not use the dt-schema
tools used by the Linux kernel). Cogeno uses bindings when generating the
EDTS database.

.. _dt-binding-compat:

Mapping nodes to bindings
*************************

During the :ref:`EDTS database build flow <devicetree-intro>`, the
:ref:`EDTS database module <cogeno_modules_edts>` tries to map each
node in the devicetree to a binding file. The
:ref:`EDTS database module <cogeno_modules_edts>` only generates
database entries for devicetree nodes which have matching bindings.
Nodes are mapped to bindings by their :ref:`compatible properties <dt-syntax>`.
Take the following node as an example:

.. code-block:: DTS

   bar-device {
   	compatible = "foo-company,bar-device";
   	/* ... */
   };

The :ref:`EDTS database module <cogeno_modules_edts>` will try to map
the ``bar-device`` node to a YAML binding with this ``compatible:`` line:

.. code-block:: yaml

   compatible: "foo-company,bar-device"

Built-in :ref:`generic bindings <cogeno_edts_bindings>` are provided by the
:ref:`EDTS database module <cogeno_modules_edts>`. Binding file
names usually match their ``compatible:`` lines, so the above binding would be
named :file:`foo-company,bar-device.yaml`.

If a node has more than one string in its ``compatible`` property, the
:ref:`EDTS database module <cogeno_modules_edts>` looks for compatible bindings
in the listed order and uses the first match. Take this node as an example:

.. code-block:: DTS

   baz-device {
   	compatible = "foo-company,baz-device", "generic-baz-device";
   };

The ``baz-device`` node would get mapped to the binding for compatible
``"generic-baz-device"`` if the build system can't find a binding for
``"foo-company,baz-device"``.

Nodes without compatible properties can be mapped to bindings associated with
their parent nodes. For an example, see the ``pwmleds`` node in the bindings
file format described below.

If a node describes hardware on a bus, like I2C or SPI, then the bus type is
also taken into account when mapping nodes to bindings. See the comments near
``on-bus:`` in the bindings syntax for details.

Bindings file syntax
********************

Below is a template (:download:`binding-template.yaml`) that shows the bindings
file syntax. It is taken from Zephyr.

.. literalinclude:: binding-template.yaml
   :language: yaml
