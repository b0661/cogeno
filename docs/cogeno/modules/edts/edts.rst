..
    Copyright (c) 2018..2020 Bobby Noelte
    SPDX-License-Identifier: Apache-2.0

.. _cogeno_modules_edts:

Extended device tree support (edts)
###################################

.. contents::
   :depth: 1
   :local:
   :backlinks: top

Description
***********

The EDTS database module extracts device tree information from the device tree
specification. See the :ref:`devicetree-guide` for more information on device tree.

The EDTS database is a key value store. The keys are pathes to the device tree information.
The EDTS database may be loaded from a json file, stored to a json file or
extracted from the DTS files and the bindings yaml files of the project. The
EDTS database is automatically available to Cogeno scripts. It can also be
used as a standalone tool.

You may get access to the database by :func:`cogeno.edts()`.

In case you want to use the extended device tree database in another Python
project import it by:

::

    import cogeno.modules.edts

Cogeno invocation options
*************************

``--edts:bindings-dirs DIR [DIR ...]``
    Use bindings from bindings DIR for device tree extraction. We allow multiple.

``--edts:bindings-exclude [DIR ...] [FILE ...]``
    Exclude bindings DIR or FILE from usage for device tree extraction. We allow multiple.

``--edts:bindings-no-default``
    Do not add EDTS database's generic bindings to bindings by default.

``--edts:db FILE``
    Write or read EDTS database to/ from FILE.

``--edts:dts FILE``
    Write (see dts-pp-sources) or read device tree specification to/ from this FILE.

``--edts:dts-pp-sources FILE [FILE ...]``
    Generate the DTS file by pre-processing the DTS source FILE(s).

``--edts:dts-pp-include-dirs DIR [DIR ...]``
    Define include DIR(s) to pre-processor.

``--edts:dts-pp-defines DEFINE [DEFINE ...]``
    DEFINE variable to pre-processor.

Code generation functions
*************************

There is a convenience function to access the EDTS database:

.. doxygenfunction:: cogeno::stdmodules::StdModulesMixin::edts()
    :project: cogeno

The EDTS database itself provides a more rich set of methods for database access:
    
.. toctree::
   :maxdepth: 1

   edts_api

Generate code with edts
***********************

.. toctree::
   :maxdepth: 3

   devicetree_guide
   devicetree_from_cogeno
   edts_bindings
