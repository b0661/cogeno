..
    Copyright (c) 2015-2020 Zephyr Project members and individual contributors
    Copyright (c) 2020 Bobby Noelte
    SPDX-License-Identifier: Apache-2.0

.. _devicetree-guide:

Devicetree Guide
################

This is a high-level guide to devicetree and how to use it with Cogeno.
See :ref:`cogeno_edts_api` for an API reference.

The guide is a partial copy of the `Zephyr devicetree guide <https://docs.zephyrproject.org/latest/guides/dts/index.html>`_
adapted to the specific devicetree capabilities of the Cogeno :ref:`EDTS database module <cogeno_modules_edts>`.

.. _Devicetree specification: https://www.devicetree.org/

.. toctree::
   :maxdepth: 2

   devicetree_intro.rst
   devicetree_bindings.rst
